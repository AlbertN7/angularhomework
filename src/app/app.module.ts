import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { BoardModule } from './board/board.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, 
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    StoreDevtoolsModule.instrument({
      maxAge: 10,
      logOnly: environment.production // Work only if app is in production mode.
    }),
    CoreModule,
    BoardModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

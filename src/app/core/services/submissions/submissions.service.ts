import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay } from 'rxjs/operators'
import { TaskItem , Status } from 'src/app/board/store/board.model';

@Injectable({
  providedIn: 'root'
})

export class SubmissionsService {

  private TASKS_URL = "http://localhost:3000/tasks" 
  private STATUS_URL = "http://localhost:3000/status" 

  constructor(private http: HttpClient){}

  getTasks(){
    return this.http.get<TaskItem[]>(this.TASKS_URL).pipe(
      delay(100)
    )
  }

  addTask(task:TaskItem){
    return this.http.post(this.TASKS_URL, task).pipe(
      delay(100)
    )
  }

  deleteTask(id:string){
    return this.http.delete(`${this.TASKS_URL}/${id}`).pipe(
      delay(100)
    )
  }

  updateTask(id:string, task:TaskItem ){
    return this.http.put(`${this.TASKS_URL}/${id}`, task).pipe(
      delay(100)
    )
  }

  getStatusList(){
    return this.http.get<Status[]>(this.STATUS_URL).pipe(
      delay(100)
    )
  }

  changeTaskStatus(task:TaskItem){
    return this.http.put(`${this.TASKS_URL}/${task.id}`, task).pipe(
      delay(100)
    )
  }

}

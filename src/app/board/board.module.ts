import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardRoutingModule } from './board-routing.module';
import { TaskViewComponent } from './containers/task-view/task-view.component';
import { StoreModule } from '@ngrx/store';
import { BoardReducer } from './store/board.reducer';
import { EffectsModule } from '@ngrx/effects';
import { BoardEffects } from './store/board.effects';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { NewTaskReducer } from './store/newTask/newTask.reducer';
import { DragDropModule} from '@angular/cdk/drag-drop'
// import { NewTaskEffect } from './store/newTask/newTask.effects';

@NgModule({
  declarations: [
    TaskViewComponent
  ],
  
  imports: [
    CommonModule,
    FormsModule,
    BoardRoutingModule,

    StoreModule.forRoot({
      board: BoardReducer,
      newTask: NewTaskReducer,
    }),
    EffectsModule.forRoot([BoardEffects]),
    
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    DragDropModule,
  ]
})
export class BoardModule { }

import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable} from 'rxjs';
import { Status, TaskItem } from '../../store/board.model';
import { AddTask, DeleteTask, LoadBoardStatus, LoadBoardTask, UpdateTask } from '../../store/board.actions';
import { BoardState } from '../../store/board.state';
import { selectBoard} from '../../store/board.selector';
import { NewTaskChangeStatus, NewTaskChangeTitle, NewTaskInitiate } from '../../store/newTask/newTask.action';
import { NewTaskState } from '../../store/newTask/newTask.state';
import { NewTask } from '../../store/newTask/newTask.reducer';
import { BoardInterface } from '../../store/board.reducer';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.sass']
})
export class TaskViewComponent implements OnInit {

  boardTaskList$!: Observable<Array<TaskItem>>
  boardStatusList$!: Observable<Array<Status>>
  loading$!: Observable<Boolean>
  error$!: Observable<String>

  board$!: Observable<BoardInterface>
  newTask$!: Observable<NewTask>

  constructor(private storeBoard: Store<BoardState>, private storeNewTask: Store<NewTaskState>) { }

  ngOnInit(){
    this.storeBoard.dispatch(LoadBoardStatus())
    this.storeBoard.dispatch(LoadBoardTask())
    this.storeNewTask.dispatch(NewTaskInitiate())

    this.board$ = this.storeBoard.pipe(select(selectBoard))
    this.newTask$ = this.storeNewTask.select(store => store.newTask)

  }

  getNewTaskStatus(status: string){
      this.storeNewTask.dispatch(NewTaskChangeStatus({payload:status}))
  }

  getNewTaskTitle(title:string){
    this.storeNewTask.dispatch(NewTaskChangeTitle({payload:title}))
  }

  addTaskToBoard(task:TaskItem){
    this.storeBoard.dispatch(AddTask({payload:task}))
    this.storeNewTask.dispatch(NewTaskInitiate())
  }

  deleteTaskFromBoard(taskId:string){
    this.storeBoard.dispatch(DeleteTask({payload:taskId}))
  }

  updateTask(task:TaskItem, status:string = '', title:string = ''){
    const newTask = {...task}
    if(title) newTask.title = title
    if(status) newTask.status = status
    this.storeBoard.dispatch(UpdateTask({payload:newTask}))
  }
  dropChangeStatus(event: CdkDragDrop<string[]>){
  

    if (event.previousContainer === event.container) {
      
    } else {

      this.updateTask(event.item.data,event.container.data[0] )
    }

  }


}

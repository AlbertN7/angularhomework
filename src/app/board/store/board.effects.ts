import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { catchError, map, switchMap } from 'rxjs/operators'
import { SubmissionsService } from '../../core/services/submissions/submissions.service'
import { of} from "rxjs";
import * as BoardActions from "./board.actions";

@Injectable()
export class BoardEffects {
    constructor(private actions: Actions , private submissionsService: SubmissionsService){}

    loadBoradTaskEffect = createEffect(() => {
        return this.actions.pipe(
        ofType(BoardActions.LoadBoardTask),
        switchMap(() => this.submissionsService.getTasks().pipe(
            map((data) => BoardActions.LoadBoardTaskSuccess({payload:data})),
            catchError(error => of(BoardActions.LoadBoardTaskFail(error)))
        ))
    )}) 

    loadBoradStatusEffect = createEffect(() => {
        return this.actions.pipe(
        ofType(BoardActions.LoadBoardStatus),
        switchMap(() => this.submissionsService.getStatusList().pipe(
            map((data) => BoardActions.LoadBoardStatusSuccess({payload:data})),
            catchError(error => of(BoardActions.LoadBoardStatusFail(error)))
        ))
    )}) 

    addTaskEffect = createEffect(() => {
        return this.actions.pipe(
        ofType(BoardActions.AddTask),
        switchMap((data) => this.submissionsService.addTask(data.payload).pipe(
            map(() => BoardActions.AddTaskSuccess({payload:data.payload})),
            catchError(error => of(BoardActions.AddTaskFail(error)))
        ))
    )}) 

    deleteTaskEffect = createEffect(() => {
        return this.actions.pipe(
        ofType(BoardActions.DeleteTask),
        switchMap((data) => this.submissionsService.deleteTask(data.payload).pipe(
            map(() => BoardActions.DeleteTaskSuccess({payload:data.payload})),
           
        ))
    )}) 

    updateTaskServerEffect = createEffect(() => 
        this.actions.pipe(
        ofType(BoardActions.UpdateTask),
        switchMap((data) => this.submissionsService.updateTask(data.payload.id, data.payload))
    ),
    { dispatch: false }) 

    updateTaskEffect = createEffect(() => 
        this.actions.pipe(
        ofType(BoardActions.UpdateTask),
        map((data) => BoardActions.UpdateTaskSuccess({payload:data.payload})),
        catchError(error => of(BoardActions.DeleteTaskFail(error)))
    )) 



}
import { BoardInterface } from "./board.reducer"

export interface BoardState{
    readonly board: BoardInterface
}

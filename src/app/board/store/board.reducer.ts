import { Action, createReducer, on } from "@ngrx/store";
import { Status, TaskItem } from "./board.model";
import * as BoardActions from "./board.actions";

export interface BoardInterface{
    taskList: TaskItem[],
    statusList: Status[],
    loading: Boolean,
    error: String,
}

const initialState: BoardInterface = {
    taskList: [],
    statusList: [],
    loading: false,
    error: '',
}

const reducer = createReducer(
    initialState,
    
    on(BoardActions.LoadBoardTask,BoardActions.LoadBoardStatus, BoardActions.AddTask, BoardActions.DeleteTask,BoardActions.UpdateTask, (state, action) => {
        return {...state, loading:true}
    }),

    //Load Tasks
    on(BoardActions.LoadBoardTaskSuccess, (state, action) => {
        return {...state, taskList:action.payload, loading:false}
    }),
    on(BoardActions.LoadBoardTaskFail, (state, action) => {
        return {...state, error: 'Load Task Error' , loading:false}
    }),

    //Load Status
    on(BoardActions.LoadBoardStatusSuccess, (state, action) => {
        return {...state, statusList:action.payload, loading:false}
    }),
    on(BoardActions.LoadBoardStatusFail, (state, action) => {
        return {...state, error: 'Load Status Error' , loading:false}
    }),

    //Add Tasks
    on(BoardActions.AddTaskSuccess, (state, action) => {
        return {...state, taskList:[...state.taskList, action.payload], loading:false}
    }),
    on(BoardActions.AddTaskFail, (state, action) => {
        return {...state, error: 'Add Task Error' , loading:false}
    }),

    //Delete Tasks
    on(BoardActions.DeleteTaskSuccess, (state, action) => {
        return {...state, taskList: state.taskList.filter(data => data.id !== action.payload), loading:false}
    }),
    on(BoardActions.DeleteTaskSuccess, (state, action) => {
        return {...state, error: 'Delete Task Error' , loading:false}
    }),

    //Update Tasks
    on(BoardActions.UpdateTaskSuccess, (state, action) => {

        const taskIndex = state.taskList.findIndex(data => data.id === action.payload.id)
        const newTaskList = [...state.taskList]
        newTaskList[taskIndex] = action.payload

        return {...state, taskList:newTaskList , loading:false}
    }),

    on(BoardActions.UpdateTaskFail, (state, action) => {
        return {...state, error: 'Delete Task Error' , loading:false}
    }),

)

export function BoardReducer(state: BoardInterface | undefined, action: Action){
    return reducer(state, action)
}
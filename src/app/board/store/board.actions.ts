import {createAction, props } from "@ngrx/store";
import { Status, TaskItem } from "./board.model";

export const LoadBoardTask = createAction(
    '[BOARD] Load Board Task'
    )

export const LoadBoardTaskSuccess = createAction(
    '[BOARD] Load Board Task Success',
    props<{payload: TaskItem[]}>()
    )

export const LoadBoardTaskFail = createAction(
    '[BOARD] Load Task Task Fail',
    props<{payload: TaskItem[]}>()
    )

export const AddTask = createAction(
    '[BOARD] Add Task',
    props<{payload: TaskItem}>()
    )

export const AddTaskSuccess = createAction(
    '[BOARD] Add Task Success',
    props<{payload: TaskItem}>()
    )

export const AddTaskFail = createAction(
    '[BOARD] Add Task Fail',
    props<{payload: TaskItem}>()
    )

export const DeleteTask = createAction(
    '[BOARD] Delete Task',
    props<{payload: string}>()
    )

export const DeleteTaskSuccess = createAction(
    '[BOARD] Delete Task Success',
    props<{payload: string}>()
    )

export const DeleteTaskFail = createAction(
    '[BOARD] Delete Task Fail',
    props<{payload: string}>()
    )

export const LoadBoardStatus = createAction(
    '[BOARD] Load Board Status'
    )

export const LoadBoardStatusSuccess = createAction(
    '[BOARD] Load Board Status Success',
    props<{payload: Status[]}>()
    )

export const LoadBoardStatusFail = createAction(
    '[BOARD] Load Task Status Fail',
    props<{payload: Status[]}>()
    )

export const UpdateTask = createAction(
    '[BOARD] Update Task',
    props<{payload: TaskItem}>()
    )

export const UpdateTaskSuccess = createAction(
    '[BOARD] Update Task Success',
    props<{payload: TaskItem}>()
    )

export const UpdateTaskFail = createAction(
    '[BOARD] Update Task Fail',
    props<{payload: TaskItem}>()
    )
    
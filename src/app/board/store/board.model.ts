export interface TaskItem{
    id: string,
    status: string,
    title: string,
}

export interface Status {
    id: string,
    color: string,
    name: string,
}
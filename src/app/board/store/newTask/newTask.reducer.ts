import { TaskItem } from "../board.model";
import { Action, createReducer, on } from "@ngrx/store";
import * as NewTaskActions from './newTask.action'
import { v4 as uuidV4 } from 'uuid';


export interface NewTask {
    id: string,
    status: string,
    title: string,
}

const initialState: NewTask = {
    id: '',
    status: '',
    title: '',
}

const reducer = createReducer(
    initialState,
    
    on(NewTaskActions.NewTaskInitiate, (state, action) => {
        return {...state, id: uuidV4()}
    }),

    on(NewTaskActions.NewTaskChangeStatus, (state, action) => {
        return {...state, status:action.payload}
    }), 

    on(NewTaskActions.NewTaskChangeTitle, (state, action) => {
        return {...state, title:action.payload }
    }),


)

export function NewTaskReducer(state: NewTask | undefined, action: Action){
    return reducer(state, action)
}
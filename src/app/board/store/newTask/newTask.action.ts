import { createAction, props } from "@ngrx/store";
import {  TaskItem } from "../board.model";

export const NewTaskInitiate = createAction(
    '[NEWTASK] Initiate NewTask'
    )

export const NewTaskChangeStatus = createAction(
    '[NEWTASK] NewTask Change Status',  
    props<{payload: string}>()
    )

export const NewTaskChangeTitle = createAction(
    '[NEWTASK] NewTask Change Title ',
    props<{payload: string}>()
    )
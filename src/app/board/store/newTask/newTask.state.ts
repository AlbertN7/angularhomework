import { NewTask } from "./newTask.reducer";

export interface NewTaskState{
    readonly newTask: NewTask
}

import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { catchError, concatAll, concatMap, first, map, switchMap } from 'rxjs/operators'
import * as NewTaskActions from "./newTask.action";
import { Store } from "@ngrx/store";
import { BoardState } from "../board.state";


@Injectable()
export class NewTaskEffect {
    constructor(private actions$: Actions, private store$: Store<BoardState>){}

    // loadBoradTaskEffect = createEffect(() => {
    //     return this.actions$.pipe(
    //     ofType(NewTaskActions.NewTaskInitiate),
    //     switchMap(() => this.store$.pipe(
    //         map(data => data.board.statusList),
    //         concatAll(),
    //         first(),
    //         map(data => NewTaskActions.NewTaskChangeStatus({payload: data.id}))
    //     ))
    // )})     

}